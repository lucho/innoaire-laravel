<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('presentation.index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

// Nuetras rutas personalizadas.abnf

Route::resource('good', 'GoodController', ['except' => ['show']]);

// Rutas Innoaire
//Route::resource('presentation', 'InnoairePresentationController', ['except' => ['show']]);

Route::get('/about-us', 'InnoairePresentationController@aboutUs');
Route::get('/presentation', 'InnoairePresentationController@index');
Route::get('/basicActivities', 'InnoairePresentationController@basicActivities');
Route::get('/ourLines', 'InnoairePresentationController@ourLines');
Route::get('/supplies', 'InnoairePresentationController@supplies');
Route::get('/contactUs', 'InnoairePresentationController@contactUs');

/**
 * Rutas para el registro de los datos de quienes somos
 */
Route::resource('whoweares', 'whoWeAreController', ['except' => ['show']]);



