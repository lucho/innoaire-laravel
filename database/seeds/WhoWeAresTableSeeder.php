<?php

use Illuminate\Database\Seeder;
use App\models\whoWeAre;

class WhoWeAresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        whoWeAre::create([
            'id' => 1,
            'text' => 'Empresa especializada en el suministro e instalación de aire acondicionado, ventilación, extracción, refrigeración en general. Plantas electricas, UPS, sistemas eléctricos de control, potencia en los sistemas mencionados en general, garantizamos la calidad técnica de nuestros servicios y la satisfacción absoluta a nuestros clientes',
            'mission' => 'Brindar soluciones que generen valor agregado a nuestros clientes y socios, en el mercado de aires acondicionados y relacionados a travez de talento humano calificado, mejoramiento continuo, uso de tecnología de alto nivel, alianzas estratégicas, responsabilidad social y buenas prácticas.',
            'vision' => 'INNOAIRE INGENIERIA ELECTRICA SAS será para el año 2025 una empresa líder del mercado con personal capacitado y comprometido, logrando alto posicionamiento para atender los diferentes sectores con cobertura a nivel nacional.',
            'social_objective' => 'El servicio, la dirección, coordinación, planeación y jecución de proyectos relacionados con sistemas de aire acondicionado, sistemas de ventilación, sistemas de calefacción, refrigeración y sistemas electricos de control y potencia, en la automatización para procesos mas faciles de aplicación en el desarrollo del país. Nos comprometemos a satisfacer al cliente a través de la calidad en nuestros servicios y la atención inmediata ante cualquier requerimiento o emergencia.'
        ]);
    }
}
