<?php

namespace App\Http\Controllers;
use App\models\whoWeAre;
use Illuminate\Http\Request;

class InnoairePresentationController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function index()
    {
        return view('presentation.index');
    }
    /**
     * Quienes somos
     */
    public function  aboutUs()
    {
        $whoweare = whoWeAre::all();
        return view('presentation.aboutUs',['whoweare' => $whoweare]);
    }

    /**
     * Actividades Básicas
     */
    public function basicActivities(){
        return view('presentation.basicActivities');
    }

    public function ourLines(){
        return view('presentation.ourLines');
    }

    public function supplies(){
        return view('presentation.supplies');
    }

    public function contactUs(){
        return view('presentation.contactUs');

    }
}