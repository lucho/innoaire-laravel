<?php

namespace App\Http\Controllers;

use App\models\whoWeAre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Exception;

class whoWeAreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $whoWeAre = whoWeAre::paginate(5);
        return view('whoweares.index', ['whoweares' => $whoWeAre]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('whoweares.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\whoWeAre  $whoWeAre
     * @return \Illuminate\Http\Response
     */
    public function show(whoWeAre $whoWeAre)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\whoWeAre  $whoWeAre
     * @return \Illuminate\Http\Response
     */
    public function edit(whoWeAre $whoWeAre)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\whoWeAre  $whoWeAre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, whoWeAre $whoWeAre)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\whoWeAre  $whoWeAre
     * @return \Illuminate\Http\Response
     */
    public function destroy(whoWeAre $whoWeAre)
    {
        //
    }
}
