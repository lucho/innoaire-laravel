<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class whoWeAre extends Model
{
    protected $fillable = [
        'image',
        'text',
        'mission',
        'vision',
        'social_objective',
        'created_at',
        'updated_at'
        ];
}
