@extends('layouts.app', ['title' => __('Crean de presentación')])
@section('css')
<link type="text/css" href="{{asset('dropzone/dropzone.min.css')}}" rel="stylesheet">
@stop
@section('content')
    @include('users.partials.header', ['title' => __('Creación de presentación')])   
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Lista de presentación') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('whoweares.index') }}" class="btn btn-sm btn-primary">{{ __('Volver a la lista') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('whoweares.store') }}" autocomplete="off">
                            @csrf
                            
                            <h6 class="heading-small text-muted mb-4">{{ __('Informacion del bien') }}</h6>
                            <div class="pl-lg-4">

                            <div class="form-group{{ $errors->has('image') ? ' has-danger' : '' }}">
                                    <form action="{{ route('whoweares.store') }}"
    class="dropzone" id="my-awesome-dropzone">
      {{ csrf_field() }}
</form> 
                                </div>

                                <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Nombre') }}</label>
                                    <input type="text" name="name" id="input-name" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Nombre') }}" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-description">{{ __('Descripción') }}</label>
                                    <textarea rows="4" cols="50" type="text" name="description" id="input-description" class="form-control form-control-alternative{{ $errors->has('description') ? ' is-invalid' : '' }}" placeholder="{{ __('Descripción') }}" value="{{ old('description') }}" required autofocus>
                                        </textarea>
                                    @if ($errors->has('description'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('value') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-value">{{ __('Valor') }}</label>
                                    <input type="number" name="value" id="input-value" class="form-control form-control-alternative{{ $errors->has('value') ? ' is-invalid' : '' }}" placeholder="{{ __('Valor') }}" value="{{ old('value') }}" required>

                                    @if ($errors->has('value'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('value') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Guardar') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> 
        <!-- <form action="{{ route('whoweares.store') }}"
    class="dropzone" id="my-awesome-dropzone">
      {{ csrf_field() }}
</form> -->
        
        
        @include('layouts.footers.auth')
    </div>
@endsection

@section('script')
<script src="{{asset('dropzone/dropzone.js')}}">

Dropzone.options.myAwesomeDropzone = {
    paramName: "file", // Las imágenes se van a usar bajo este nombre de parámetro
    maxFilesize: 2, // Tamaño máximo en MB
    success: function (file, response) {
        console.log(response);
    }
};
</script>


@endsection