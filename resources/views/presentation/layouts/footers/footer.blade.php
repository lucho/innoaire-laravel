<footer class="footer-main">
            <div class="footer-bottom">
                <div class="container clearfix">
                    <div class="copyright-text">
                        <p>&copy; Copyright 2019. LEGA solutions in software design
                            <!-- <a href="index.html">Medic</a> -->
                        </p>
                    </div>
                    <ul class="footer-bottom-link">
                        <li class="item">
                            <p>Comunícate con nosotros</p>
                            <a>
                                <p>TV 24 B 17 307 TO 2 AP 404</p>
                            </a>
                        </li>
                        <li class="item">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            <a href="#">
                                <p>contactenos@innoaire.com.co</p>
                            </a>
                        </li>
                        <li class="item">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <p>3133549378</p>
                        </li>
                    </ul>
                </div>
            </div>
        </footer>