<nav class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active ">
                    <a href="/presentation"><i class="fa fa-home" aria-hidden="true"></i> Inicio</a>
                </li>
                <li>
                    <a href="/about-us">Quienes Somos</a>
                </li>
                <li>
                    <a href="/basicActivities">Actividades Básicas</a>
                </li>
                <li>
                    <a href="/ourLines">Nuestras Líneas</a>
                </li>

                <li>
                    <a href="/supplies">Suministros</a>
                </li>
                <li>
                    <a href="/contactUs">Contáctenos</a>
                </li>

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>