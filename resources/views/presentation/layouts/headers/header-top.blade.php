<div class="header-top">
    <div class="container clearfix">
        <div class="top-left">
            <!-- <h6>Opening Hours : Saturday to Tuesday - 8am to 10pm</h6> -->
        </div>
        <div class="top-right">
            <ul class="list-group list-group-horizontal">
                <li class="list-group-item">
                    <a href="/about-us">
                        <i class="fa " aria-hidden="true">Historia</i>
                    </a></li>
                <li class="list-group-item">
                    <a href="/ourLines">
                        <i class="fa " aria-hidden="true">Proyecto</i>
                    </a></li>
                <li class="list-group-item">
                    <a href="/supplies">
                        <i class="fa " aria-hidden="true">Producto</i>
                    </a></li>
                <li class="list-group-item">
                    <a href="/contactUs">
                        <i aria-hidden="true">Contacto</i>
                    </a>
                </li>
                <li class="list-group-item">
                    <a href="/home">
                        <i class="fa fa-user-circle-o" aria-hidden="true"> Iniciar Sesión</i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>