<section class="header-uper">
    <div class="container clearfix">
        <div class="logo">
            <figure>
                <a href="index.html">
                    <img src="{{asset('innoaire/images/logo.png')}}" alt="" width="330">
                </a>
            </figure>
        </div>
        <div class="right-side">
            <ul class="contact-info">
                <li class="item">
                    <div class="icon-box">
                        <i class="fa fa-envelope-o"></i>
                    </div>
                    <strong>Email</strong>
                    <br>
                    <a href="#">
                        <span>contactenos@innoaire.com.co</span>
                    </a>
                </li>
                <li class="item">
                    <div class="icon-box">
                        <i class="fa fa-phone"></i>
                    </div>
                    <strong>Telefonos</strong>
                    <br>
                    <span> 3133549378</span>
                </li>
            </ul>

        </div>
    </div>
</section>