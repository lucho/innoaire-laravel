@extends('presentation.home')
@section('content')
<div class="hero-slider">
    <!-- Slider Item -->
    <div class="slider-item slide1" style="background-image:url(innoaire/images/innoaire/inoairebanner1.png)">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Slide Content Start -->
                    <div class="content style text-center">
                        <h2 class="text-white text-bold mb-2">Quienes Somos</h2>
                        <p class="tag-text mb-5">nuestro espíritu</p>
                        <a href="about.html" class="btn btn-main btn-white">Ir</a>
                    </div>
                    <!-- Slide Content End -->
                </div>
            </div>
        </div>
    </div>
    <!-- Slider Item -->
    <div class="slider-item" style="background-image:url(innoaire/images/innoaire/innoaire1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Slide Content Start-->
                    <div class="content style text-center">
                        <h2 class="text-white text-bold mb-2">Líneas</h2>
                        <p class="tag-text">Nuestras líneas de servicio</p>
                        <a href="blog.html" class="btn btn-main btn-white">Ir</a>
                    </div>
                    <!-- Slide Content End-->
                </div>
            </div>
        </div>
    </div>
    <!-- Slider Item -->
    <div class="slider-item" style="background-image:url(innoaire/images/innoaire/innoaire2.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Slide Content Start -->
                    <div class="content text-center style">
                        <h2 class="text-white text-bold mb-2">Suministros</h2>
                        <p class="tag-text mb-5">Sistemas de aire acondicionado en general (mini split, split, multi inverter, sistemas centrales de expansión directa, sistemas de agua fría chiller, sistemas variables y paquetes industriales).</p>
                        <a href="appointment.html" class="btn btn-main btn-white">Ir</a>
                    </div>
                    <!-- Slide Content End -->
                </div>
            </div>
        </div>
    </div>
</div>

<!--====  End of Page Slider  ====-->

<section class="cta">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="cta-block">
                    <div class="emmergency item">
                        <i class="fa fa-handshake-o"></i>
                        <h2>Aliados</h2>
                        <!-- <a href="#">1-800-700-6200</a> -->
                        <p>Contamos con varios aliados que fortalecen nuestros procesos</p>
                    </div>
                    <div class="top-doctor item">
                        <i class="fa fa-calendar"></i>
                        <h2>Horario</h2>
                        <p>Tenemos horarios de atencion muy flexibles, acorde a la necesidad de nuestros clientes. Cubrimos varias zonas del país.
                        </p>
                        <a href="service.html" class="btn btn-main">ver zonas</a>
                    </div>
                    <div class="working-time item">
                        <i class="fa fa-money"></i>
                        <h2>Precio</h2>
                        <p>Nuestros precios son los mas accesibles</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--Start about us area-->
<section class="service-tab-section section">
    <div class="outer-box clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End about us area-->
<!--Service Section-->
<section class="service-section bg-gray section">
    <div class="container">
        <div class="section-title text-center">
            <h3>Galería
                <!-- <span>Services</span> -->
            </h3>
            <p>Nuestro esfuerzo reflejado
            </p>
        </div>
        <div class="row items-container clearfix">
            <div class="item">
                <div class="inner-box">
                    <div class="img_holder">
                        <a href="service.html">
                            <img src="{{asset('innoaire/images/sinfoto.jpeg')}}" alt="images" class="img-responsive">
                        </a>
                    </div>
                    <div class="image-content text-center">
                        <!-- <span>Better Service At Low Cost</span>
                                <a href="service.html">
                                    <h6>Dormitory</h6>
                                </a>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit, vero.</p> -->
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="inner-box">
                    <div class="img_holder">
                        <a href="service.html">
                            <img src="{{asset('innoaire/images/sinfoto.jpeg')}}" alt="images" class="img-responsive">
                        </a>
                    </div>
                    <div class="image-content text-center">
                        <!-- <span>Better Service At Low Cost</span>
                                <a href="service.html">
                                    <h6>Germs Protection</h6>
                                </a>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit, vero.</p> -->
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="inner-box">
                    <div class="img_holder">
                        <a href="service.html">
                            <img src="{{asset('innoaire/images/sinfoto.jpeg')}}" alt="images" class="img-responsive">
                        </a>
                    </div>
                    <div class="image-content text-center">
                        <!-- <span>Better Service At Low Cost</span>
                                <a href="service.html">
                                    <h6>Psycology</h6>
                                </a>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit, vero.</p> -->
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="inner-box">
                    <div class="img_holder">
                        <a href="service.html">
                            <img src="{{asset('innoaire/images/sinfoto.jpeg')}}" alt="images" class="img-responsive">
                        </a>
                    </div>
                    <div class="image-content text-center">
                        <!-- <span>Better Service At Low Cost</span>
                                <a href="service.html">
                                    <h6>Dormitory</h6>
                                </a>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit, vero.</p> -->
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="inner-box">
                    <div class="img_holder">
                        <a href="service.html">
                            <img src="{{asset('innoaire/images/sinfoto.jpeg')}}" alt="images" class="img-responsive">
                        </a>
                    </div>
                    <div class="image-content text-center">
                        <!-- <span>Better Service At Low Cost</span>
                                <a href="service.html">
                                    <h6>Germs Protection</h6>
                                </a>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit, vero.</p> -->
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="inner-box">
                    <div class="img_holder">
                        <a href="service.html">
                            <img src="{{asset('innoaire/images/sinfoto.jpeg')}}" alt="images" class="img-responsive">
                        </a>
                    </div>
                    <div class="image-content text-center">
                        <!-- <span>Better Service At Low Cost</span>
                                <a href="service.html">
                                    <h6>Psycology</h6>
                                </a>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit, vero.</p> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Service Section-->

@endsection