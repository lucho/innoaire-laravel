<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <title>INNOAIRE INGENIERIA ELECTRICA S.A.S</title>
    <!-- mobile responsive meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Slick Carousel -->
    <link rel="stylesheet" href="{{asset('innoaire/plugins/slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('innoaire/plugins/slick/slick-theme.css')}}">
    <!-- FancyBox -->
    <link rel="stylesheet" href="{{asset('innoaire/plugins/fancybox/jquery.fancybox.min.css')}}">
    <!-- Stylesheets -->
    <link href="{{asset('innoaire/css/style.css')}}" rel="stylesheet">
    <!--Favicon-->
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('innoaire/images/favicon.ico')}}" type="image/x-icon">
</head>

<body>

    <div class="page-wrapper">
        <!--header top-->
        @include('presentation.layouts.headers.header-top')
        <!--End header top-->

        <!--Header Upper-->
        @include('presentation.layouts.headers.header-upper')
        <!--End Header Upper-->
        <!--Main Header-->
        @include('presentation.layouts.headers.header-main')
        <!--End Main Header -->
        <!--=================================
==================================-->
        @yield('content')
        <!--=================================
==================================-->
     

        <!--footer-main-->
        @include('presentation.layouts.footers.footer')
        <!--End footer-main-->

    </div>
    <!--End pagewrapper-->
    <!--Scroll to top-->
    <div class="scroll-to-top scroll-to-target" data-target=".header-top">
        <span class="icon fa fa-angle-up"></span>
    </div>
    <script src="{{asset('innoaire/plugins/jquery.js')}}"></script>
    <script src="{{asset('innoaire/plugins/bootstrap.min.js')}}"></script>
    <script src="{{asset('innoaire/plugins/bootstrap-select.min.js')}}"></script>
    <!-- Slick Slider -->
    <script src="{{asset('innoaire/plugins/slick/slick.min.js')}}"></script>
    <!-- FancyBox -->
    <script src="{{asset('innoaire/plugins/fancybox/jquery.fancybox.min.js')}}"></script>
    <!-- Google Map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCC72vZw-6tGqFyRhhg5CkF2fqfILn2Tsw"></script>
    <script src="{{asset('innoaire/plugins/google-map/gmap.js')}}"></script>
    <script src="{{asset('innoaire/plugins/validate.js')}}"></script>
    <script src="{{asset('innoaire/plugins/wow.js')}}"></script>
    <script src="{{asset('innoaire/plugins/jquery-ui.js')}}"></script>
    <script src="{{asset('innoaire/plugins/timePicker.js')}}"></script>
    <script src="{{asset('innoaire/js/script.js')}}"></script>
    @stack('js')
</body>

</html>