@extends('presentation.home')
@section('content')
<!--Page Title-->
<section class="page-title text-center" style="background-image:url(innoaire/images/innoaire/banner/banner3.jpg);">
    <div class="container">
        <div class="title-text">
            <h1>Actividades Básicas</h1>
            <!-- <ul class="title-menu clearfix">
                        <li>
                            <a href="index.html">home &nbsp;/</a>
                        </li>
                        <li>service</li>
                    </ul> -->
        </div>
    </div>
</section>
<!--End Page Title-->

<section class="service-overview section">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="content-block">
                    <h2>INOAIRE INGENIERIA <br>ELECTRICA S.A.</h2>
                    <p>nos dedicamos a las siguientes líneas de negocio en diferentes sectores del mercado:
                    </p>

                </div>
                <div class="content-block">
                    <p>Prestamos cubrimiento a las principales ciudades del país y sus municipios aledaños:</p>
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Departamento</th>
                                <th>Ciudad / Mcpio</th>
                                <th>Departamento</th>
                                <th>Ciudad / Mcpio</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Amazonas</td>
                                <td>Leticia</td>
                                <td>Huila</td>
                                <td>Neiva</td>
                            </tr>
                            <tr>
                                <td>Antioquia</td>
                                <td>Caucasia</td>
                                <td>Huila</td>
                                <td>Pitalito</td>
                            </tr>
                            <tr>
                                <td>Antioquia</td>
                                <td>Medellin</td>
                                <td>Magdalena</td>
                                <td>Santa marta</td>
                            </tr>
                            <tr>
                                <td>Atlantico</td>
                                <td>Barranquilla</td>
                                <td>Meta</td>
                                <td>Villavicencio</td>
                            </tr>
                            <tr>
                                <td>Bogotá D. C</td>
                                <td>Bogotá</td>
                                <td>Nariño</td>
                                <td>Pasto</td>
                            </tr>
                            <tr>
                                <td>Bolivar</td>
                                <td>Cartagena</td>
                                <td>Norte de santander</td>
                                <td>Cucuta</td>
                            </tr>
                            <tr>
                                <td>Boyaca</td>
                                <td>Duitama</td>
                                <td>Quindio</td>
                                <td>Armenia</td>
                            </tr>
                            <tr>
                                <td>Boyaca</td>
                                <td>Sogamoso</td>
                                <td>Risaralda</td>
                                <td>Pereira</td>
                            </tr>
                            <tr>
                                <td>Boyaca</td>
                                <td>Tunja</td>
                                <td>San andres</td>
                                <td>San andres</td>
                            </tr>
                            <tr>
                                <td>Caldas</td>
                                <td>Manizales</td>
                                <td>Santander</td>
                                <td>Barrancabermeja</td>
                            </tr>
                            <tr>
                                <td>Casanare</td>
                                <td>Yopal</td>
                                <td>Santander</td>
                                <td>Bucaramanga</td>
                            </tr>
                            <tr>
                                <td>Cauca</td>
                                <td>Popayan</td>
                                <td>Santander</td>
                                <td>Floridablanca</td>
                            </tr>
                            <tr>
                                <td>Cesar</td>
                                <td>Valledupar</td>
                                <td>Santander</td>
                                <td>San gil</td>
                            </tr>
                            <tr>
                                <td>Cordoba</td>
                                <td>Monteria</td>
                                <td>Sucre</td>
                                <td>Sincelejo</td>
                            </tr>
                            <tr>
                                <td>Cundinamarca</td>
                                <td>Chia</td>
                                <td>Valle del cauca</td>
                                <td>Buenaventura</td>
                            </tr>
                            <tr>
                                <td>Cundinamarca</td>
                                <td>Girardot</td>
                                <td>Valle del cauca</td>
                                <td>Buga</td>
                            </tr>
                            <tr>
                                <td>Cundinamarca</td>
                                <td>Mosquera</td>
                                <td>Valle del cauca</td>
                                <td>Cali</td>
                            </tr>
                            <tr>
                                <td>Cundinamarca</td>
                                <td>Zipaquira</td>
                                <td>Valle del cauca</td>
                                <td>Cartago</td>
                            </tr>
                            <tr>
                                <td>Guajira</td>
                                <td>Riohacha</td>
                                <td>Valle del cauca</td>
                                <td>Palmira</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-6">
                <div class="accordion-section">
                    <div class="accordion-holder">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            1
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        Nos comprometemos en la creación de ambientes confortables en sistemas adecuados para las diferentes necesidades, ofreciendo la mejor calidad del aire, tanto en lo relativo a su temperatura, como a su humedad y limpieza del ambiente, sistemas de plantas
                                        eléctricas, UPS, transferencias eléctricas, tableros de control y distribución eléctrico, cableado estructura y sistemas eléctricos para un ambiente confortable.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            2
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        Los equipos que incluyen nuestras soluciones son de las mejores y más reconocidas marcas que garantizan confiabilidad y duración.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            3
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        Realizamos diseños de alta ingeniería orientados a ofrecer la solución que mejor se adapte a la necesidad de cada cliente.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="fsdfsdfs">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThjhg" aria-expanded="false" aria-controls="collapseThree">
                                            4
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThjhg" class="panel-collapse collapse" role="tabpanel" aria-labelledby="fsdfsdfs">
                                    <div class="panel-body">
                                        Promovemos protección y conservación a los lugares donde se instalan los equipos que requieren condiciones ambientales especiales para la preservación y optimización de procesos de aplicación.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="gdfgdg">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseJgjhgd" aria-expanded="false" aria-controls="collapseThree">
                                            5
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseJgjhgd" class="panel-collapse collapse" role="tabpanel" aria-labelledby="gdfgdg">
                                    <div class="panel-body">
                                        Somos pioneros en la buena aplicación de la ingeniera, diseño e implementación tecnológica de los sistemas de aire acondicionado.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="gdfgdfhgdfger">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFjhk" aria-expanded="false" aria-controls="collapseThree">
                                            6
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFjhk" class="panel-collapse collapse" role="tabpanel" aria-labelledby="gdfgdfhgdfger">
                                    <div class="panel-body">
                                        Brindamos servicios de apoyo, soporte y asesoría técnica especializada.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="bfhbgfdg">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseAhjkhk" aria-expanded="false" aria-controls="collapseThree">
                                            7
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseAhjkhk" class="panel-collapse collapse" role="tabpanel" aria-labelledby="bfhbgfdg">
                                    <div class="panel-body">
                                        Nos caracterizamos por la seriedad que nos lleva al cumplimiento, responsabilidad y compromiso en los negocios.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-block">
                    <p>Sistemas de Aire Acondicionad y Ventilación Mecánica para los siguientes sectores:
                    </p>
                    <ul>
                        <li><i class="fa fa-caret-right"></i>Industrial.</li>
                        <li><i class="fa fa-caret-right"></i>Educativo.</li>
                        <li><i class="fa fa-caret-right"></i>Medio de comunicaciones.</li>
                        <li><i class="fa fa-caret-right"></i>Construcción.</li>
                        <li><i class="fa fa-caret-right"></i>Comunicaciones</li>
                        <li><i class="fa fa-caret-right"></i>Hotelero</li>
                        <li><i class="fa fa-caret-right"></i>Farmacéutico</li>
                        <li><i class="fa fa-caret-right"></i>Turismo</li>
                        <li><i class="fa fa-caret-right"></i>Minero y energético</li>
                        <li><i class="fa fa-caret-right"></i>Salud</li>
                        <li><i class="fa fa-caret-right"></i>Gubernamental</li>

                    </ul>
                </div>

            </div>
        </div>
    </div>
</section>


@endsection