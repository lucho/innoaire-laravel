@extends('presentation.home')
@section('content')

  <!--Page Title-->
  <section class="page-title text-center" style="background-image:url(innoaire/images/innoaire/innoaire2.jpg);">
            <div class="container">
                <div class="title-text">
                    <h1>Nuestras Líneas</h1>
                </div>
            </div>
        </section>
        <!--End Page Title-->

        <!-- Contact Section -->
        <section class="blog-section style-four section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="left-side">
                            <div class="item-holder">
                                <div class="image-box">
                                    <figure>
                                        <a><img src="images/logo.png" alt=""></a>
                                    </figure>
                                </div>
                                <div class="content-text">
                                    <a>
                                        <h6>En un Marco de aplicación que abarca:</h6>
                                    </a>
                                    <ul>
                                        <li><i class="fa fa-caret-right"></i> Sistemas de aire acondicionado convencionales, variables e industriales.
                                        </li>
                                        <li><i class="fa fa-caret-right"></i> Ventilación mecánica e industrial.
                                        </li>
                                        <li><i class="fa fa-caret-right"></i> Sistemas de filtración de aire para ambientes especiales.</li>
                                        <li><i class="fa fa-caret-right"></i> Extractores axiales de aire.
                                        </li>
                                        <li><i class="fa fa-caret-right"></i> Ventiladores centrífugos, ductos, rejillas, difusores, etc</li>
                                        <li><i class="fa fa-caret-right"></i> Sistemas de Plantas eléctricas, UPS.</li>
                                        <li><i class="fa fa-caret-right"></i> Transferencias eléctricas de media y baja tensión.</li>
                                        <li><i class="fa fa-caret-right"></i> Tableros eléctricos de control y distribución eléctrica, celdas eléctricas.
                                        </li>
                                        <li><i class="fa fa-caret-right"></i> Sistema de electricos de media y baja tensión, cableado estructural</li>
                                        <li><i class="fa fa-caret-right"></i> Servicio de mantenimiento integral para este tipo de instalaciones
                                        </li>
                                        <li><i class="fa fa-caret-right"></i> Obras civiles, acometidas eléctricas y de control complementarias para la instalación de los sistemas en mención</li>
                                        <li><i class="fa fa-caret-right"></i> Suministro, instalación e implementación de sistemas de control.
                                        </li>
                                        <li><i class="fa fa-caret-right"></i> Diseño, suministro e instalación de sistemas aplicados y especiales de acuerd.
                                        </li>
                                        <li><i class="fa fa-caret-right"></i> con las necesidades del cliente o proyect.</li>

                                    </ul>
                                </div>
                            </div>



                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="right-side">
                            <div class="categorise-menu">
                                <div class="text-title">
                                    <h6>Nuestras Líneas de Negocio incluye</h6>
                                </div>
                                <ul class="categorise-list">
                                    <li><a href="#">Consultorías <span>(diseños e
                                                                        interventorías)</span></a></li>
                                    <li><a href="#">Proyectos <span>(ejecución de obras)</span></a>
                                    </li>
                                    <li><a href="#">Servicios <span>(mantenimiento preventivo y
                                                                        correctivo)</span></a></li>
                                    <li><a href="#">Unitarios <span>(suministro de equipos y sistemas
                                                                        para clientes corporativos)</span></a></li>
                                </ul>
                            </div>
                            <div class="categorise-menu">
                                <div class="text-title">
                                    <h6>Mercado de intervención</h6>
                                </div>
                                <ul class="categorise-list">
                                    <li><a href="#">Consultorías <span>(diseños e
                                                                        interventorías)</span></a></li>
                                    <li><a href="#">Proyectos <span>(ejecución de obras)</span></a>
                                    </li>
                                    <li><a href="#">Servicios <span>(mantenimiento preventivo y
                                                                        correctivo)</span></a></li>
                                    <li><a href="#">Unitarios <span>(suministro de equipos y sistemas
                                                                        para clientes corporativos)</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="styled-pagination">
                    <ul>
                        <li><a class="prev" href="#"><span class="fa fa-angle-left"
                                                      aria-hidden="true"></span></a></li>
                        <li><a href="#" class="active">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a class="next" href="#"><span class="fa fa-angle-right"
                                                      aria-hidden="true"></span></a></li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- End Contact Section -->
@endsection