@extends('presentation.home')
@section('content')
<!--Pge Title-->
<section class="page-title text-center" style="background-image:url(innoaire/images/innoaire/banner/banner4.jpg);">
    <div class="container">
        <div class="title-text">
            <h1>Suministros</h1>

        </div>
    </div>
</section>
<!--End Page Title-->

<!-- Contact Section -->
<section class="blog-section section style-three pb-0">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="right-side">
                    <div class="categorise-menu">
                        <div class="text-title">
                            <h6>Suministros de</h6>
                        </div>
                        <ul class="categorise-list">
                            <li><a href="#">Sistemas de aire acondicionado en genera</a>
                            </li>
                            <li><a>Sistemas de precisión </span></a>
                            </li>
                            <li><a>Sistemas de acondicionamiento evaporativo y/o
                                    lavadores de aire</a></li>
                            <li><a>Sistemas colectores de polvo</a></li>
                            <li><a>Sistemas de filtraciones de aire</a></li>
                            <li><a>Plantas eléctricas, UPS</a></li>
                            <li><a>Transferencias eléctricas de media y baja tensión</a></li>
                            <li><a>Tableros eléctricos de control y distribución eléctrica, celdas eléctricas</a></li>


                        </ul>
                    </div>

                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="appointment-image-holder">
                    <figure>
                        <img src="{{asset('innoaire/images/innoaire/innoaire3.jpg')}}" alt="Appointment">
                    </figure>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Contact Section -->

<!--team section-->

<!--End team section-->
@endsection