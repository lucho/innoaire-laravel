@extends('presentation.home')
@section('content')
<!--Page Title-->
<section class="page-title text-center" style="background-image:url(innoaire/images/innoaire/banner/banner2.jpg);">
    <div class="container">
        <div class="title-text">
            <h1>Quienes somos</h1>
            <!-- <ul class="title-menu clearfix">
                        <li>
                            <a href="index.html">home &nbsp;/</a>
                        </li>
                        <li>about us</li>
                    </ul> -->
        </div>
    </div>
</section>
<!--End Page Title-->
<!-- Our Story -->
<section class="story">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="{{asset('innoaire/images/innoaire/inoairebanner1.png')}}" class="responsive" alt="story">
            </div>
            <div class="col-md-6">
                <div class="story-content">
                    <!-- <h2>Our Story</h2> -->
                    <!-- <h5 class="tagline">"Empresa especializada en el suministro e instalación de aire acondicionado, ."</h6> -->
                    <p>{{$whoweare[0]->text}}
                        
                    </p>
                    <h6>Misión</h6>
                    <p>{{$whoweare[0]->mission}}</p>
                    <h6>Visión</h6>
                    <p>{{$whoweare[0]->vision}}</p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Contact Section -->
<section class="appoinment-section section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="accordion-section">
                    <div class="accordion-holder">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Objetivo social
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                    {{$whoweare[0]->social_objective}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Contact Section -->

@endsection