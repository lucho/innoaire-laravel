@extends('presentation.home')
@section('content')

<!--Page Title-->
<section class="page-title text-center" style="background-image:url(innoaire/images/innoaire/banner/banner5.png);">
    <div class="container">
        <div class="title-text">
            <h1>Contacto</h1>

        </div>
    </div>
</section>
<!--End Page Title-->

<!--==================================
=            Contact Form            =
===================================-->
<section class="section contact">
    <!-- container start -->
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <!-- address start -->
                <div class="address-block">
                    <!-- Location -->
                    <div class="media">
                        <i class="fa fa-map-o"></i>
                        <div class="media-body">
                            <h3>Dirección</h3>
                            <p>Transversal 24 B # 17-307 - Bogotá
                                <!-- <br>Victoria 8007 Canada</p> -->
                        </div>
                    </div>
                    <!-- Phone -->
                    <div class="media">
                        <i class="fa fa-phone"></i>
                        <div class="media-body">
                            <h3>Teléfono</h3>
                            <p>
                                3133549378
                            </p>
                        </div>
                    </div>
                    <!-- Email -->
                    <div class="media">
                        <i class="fa fa-envelope-o"></i>
                        <div class="media-body">
                            <h3>Email</h3>
                            <p>
                                contactenos@innoaire.com.co
                            </p>
                        </div>
                    </div>
                </div>
                <!-- address end -->
            </div>
            <div class="col-md-8">
                <div class="contact-form">
                    <!-- contact form start -->
                    <form action="#" class="row">
                        <!-- name -->
                        <div class="col-md-6">
                            <input type="text" name="name" class="form-control main" placeholder="Nombre" required>
                        </div>
                        <!-- email -->
                        <div class="col-md-6">
                            <input type="email" class="form-control main" placeholder="Correo" required>
                        </div>
                        <!-- phone -->
                        <div class="col-md-12">
                            <input type="text" class="form-control main" placeholder="Teléfono" required>
                        </div>
                        <!-- message -->
                        <div class="col-md-12">
                            <textarea name="message" rows="15" class="form-control main" placeholder="Mensaje"></textarea>
                        </div>
                        <!-- submit button -->
                        <div class="col-md-12 text-right">
                            <button class="btn btn-style-one" type="submit">Enviar</button>
                        </div>
                    </form>
                    <!-- contact form end -->
                </div>
            </div>
        </div>
    </div>
    <!-- container end -->
</section>
<!--====  End of Contact Form  ====-->

<!--================================
=            Google Map            =
=================================-->
<section class="map">
    <!-- Google Map -->
    <div id="map"></div>
</section>
<!--====  End of Google Map  ====-->
@endsection