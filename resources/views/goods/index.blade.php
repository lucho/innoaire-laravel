@extends('layouts.app', ['title' => __('Lista de Bienes')])

@section('content')
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row">

                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Bienes') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('good.create') }}" class="btn btn-sm btn-primary">{{ __('Crear Bien') }}</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{ __('Nombre') }}</th>
                                    <th scope="col">{{ __('Descripción') }}</th>
                                    <th scope="col">{{ __('Valor') }}</th>
                                    <th scope="col">{{ __('Fecha de creación') }}</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($goods as $good)
                                    <tr>
                                        <td>{{ $good->name }}</td>
                                        <td>{{ $good->description }}</td>
                                        <td>{{ $good->value }}</td>
                                        <td>{{ $good->created_at->format('d/m/Y H:i') }}</td>
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <form action="{{ route('good.destroy', $good) }}" method="post">
                                                            @csrf
                                                            @method('delete')
                                                            <a class="dropdown-item" href="{{ route('good.edit', $good) }}">{{ __('Actualizar') }}</a>
                                                            <button type="button" class="dropdown-item" onclick="confirm('{{ __(" Estás seguro de que deseas eliminar a este BIEN?") }}') ? this.parentElement.submit() : ''">
                                                                {{ __('Eliminar') }}
                                                            </button>
                                                        </form>    
                                                    
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            {{ $goods->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
            
        @include('layouts.footers.auth')
    </div>
@endsection